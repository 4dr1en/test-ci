FROM php:8.0-fpm

COPY --from=composer/composer:latest-bin /composer /usr/bin/composer

RUN apt update && apt install -y g++ git zip libzip-dev zlib1g-dev libicu-dev \
    libmagickwand-dev

RUN docker-php-ext-install intl opcache pdo pdo_mysql mysqli zip gd exif

RUN pecl install apcu xdebug imagick \
    && docker-php-ext-enable apcu xdebug imagick

# Set working directory
WORKDIR /var/www/html

# Install Bedrock
RUN composer create-project roots/bedrock /var/www/html/bedrock

# Set ownership and permissions
RUN chown -R www-data:www-data /var/www/html/bedrock && \
    find /var/www/html/bedrock -type d -exec chmod 755 {} \; && \
    find /var/www/html/bedrock -type f -exec chmod 644 {} \;
